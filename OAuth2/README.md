# OAuth2 API - B1 Assignment

## Endpoint API

⚠️ **if you want to access this endpoint, u must be have a** `client_id`, `client_secret`, `authorization_code`⚠️
<br>
please contact [Privy](https://privy.id/) to get access this endpoint!

| Method | Endpoint                               | Description     |
| ------ | -------------------------------------- | --------------- |
| POST   | `OAUTH2_PRIVY_URL`/oauth/token         | get access_code |
| GET    | `MERCHENT_PRIVY_URL`/v1/users/identity | get identidy    |

## Screenshoot

### Sequential Diagram

![Sequential Diagram](sequential-diagram/OAuth2%20PrivyPass%20-%20Merchant.drawio.png)

### Get Authorization Code

![Get Authorization Code](screenshot/authorization_code.png)

### Get Access Token

![Get Access Token](screenshot/get_access_token_with_authorization_code.png)

### Get Identity

![Get Access Token](screenshot/get_identity.png)
